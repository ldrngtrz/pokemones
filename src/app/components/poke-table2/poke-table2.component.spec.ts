import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokeTableComponent2 } from './poke-table2.component';

describe('PokeTableComponent2', () => {
  let component: PokeTableComponent2;
  let fixture: ComponentFixture<PokeTableComponent2>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PokeTableComponent2 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokeTableComponent2);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
