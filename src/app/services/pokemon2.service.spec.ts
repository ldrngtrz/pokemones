import { TestBed } from '@angular/core/testing';

import { PokemonService2 } from './pokemon2.service';

describe('PokemonService2', () => {
  let service: PokemonService2;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PokemonService2);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
