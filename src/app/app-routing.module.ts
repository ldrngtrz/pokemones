import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokeDetailComponent } from './components/poke-detail/poke-detail.component';
import { PokeTableComponent2 } from './components/poke-table2/poke-table2.component';


const routes: Routes = [
  {path: 'home', component: PokeTableComponent2},
  {path: 'home2', component: PokeTableComponent2},
  {path: 'pokeDetail/:id', component: PokeDetailComponent},
  {path: '', pathMatch: 'full', redirectTo: 'home2' },
  {path: '**', pathMatch: 'full', redirectTo: 'home2'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
